<?php

namespace Hestec\RatingComments;

use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Control\Director;
use SilverStripe\View\Requirements;

class RatingCommentsControllerExtension extends DataExtension {

    private static $allowed_actions = array(
        'RatingCommentForm',
        'AddRatingComment'
    );

    public function onAfterInit() {
        //parent::init();

        if ( Director::isLive() ){
            Requirements::javascript('hestec/silverstripe-ratingcomments: client/javascript/ratingcomments.min.js');
        }else{
            Requirements::javascript('hestec/silverstripe-ratingcomments: client/javascript/ratingcomments.js');
        }

    }

    public function RatingCommentForm($name = null){

        if (empty($name)){
            $name = '';
        }

        $RatingSource = array(
            5 => '<span class="star" title="uitstekend"></span>',
            4 => '<span class="star mr-1" title="goed"></span>',
            3 => '<span class="star mr-1" title="gemiddeld"></span>',
            2 => '<span class="star mr-1" title="matig"></span>',
            1 => '<span class="star mr-1" title="slecht"></span>',
        );

        $NameField = TextField::create('Name', false);
        $NameField->setAttribute('placeholder', _t("RatingComments.NAME", "Name"));
        $EmailField = EmailField::create('Email', false);
        $EmailField->setAttribute('placeholder', _t("RatingComments.EMAIL", "Email"));
        $RatingField = OptionsetField::create('Rating', _t("RatingComments.RATING", "How many stars do you give")." ".$name."?", $RatingSource);
        $RatingField->setTemplate('Hestec\RatingComments\RatingOptionSetField');
        $RatingField->addExtraClass('mt-2');
        $CommentField = TextareaField::create('Comment', false);
        $CommentField->setAttribute('placeholder', _t("RatingComments.COMMENT", "Comment")." ".$name);
        $CommentField->addExtraClass('mt-3 mb-2');

        $IpField = HiddenField::create('Ip', 'Ip', $_SERVER['REMOTE_ADDR']);
        $PageIdField = HiddenField::create('PageId', 'PageId', $this->owner->ID);
        $LinkField = HiddenField::create('Link', 'Link', $this->owner->Link());

        $Action = FormAction::create('SubmitRatingCommentForm', _t("RatingComments.SUBMIT", "Submit"));

        $fields = FieldList::create(array(
            $NameField,
            $EmailField,
            $RatingField,
            $CommentField,
            $IpField,
            $PageIdField,
            $LinkField
        ));

        $actions = FieldList::create(
            $Action
        );

        $form = Form::create($this->owner, __FUNCTION__, $fields, $actions);
        $form->enableSpamProtection();

        return $form;

    }

    public function SubmitRatingCommentForm($data,$form)
    {

        return $this->owner->redirectBack();

    }

    public function AddRatingComment()
    {

        if (isset($_GET['ip']) && isset($_GET['pageid'])) {

            $output = array();

            $today = new \DateTime('today');

            $ipcount = RatingComment::get()->filter(array('Created:GreaterThanOrEqual' => $today->format('Y-m-d H:i:s'), 'Ip' => $_GET['ip']))->count();

            if ($ipcount > 3) {
                $output['status'] = "error";
                $output['message'] = "Je kunt maximaal 3 beoordelingen per dag geven.";
            } elseif (!isset($_GET['name']) || strlen($_GET['name']) < 2) {
                $output['status'] = "error";
                $output['message'] = "Controleer nog even je naam.";
            } elseif (!isset($_GET['email']) || !filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) {
                $output['status'] = "error";
                $output['message'] = "Controleer nog even je e-mailadres.";
            } elseif (!isset($_GET['rating']) || $_GET['rating'] == 0) {
                $output['status'] = "error";
                $output['message'] = "Geef een beoordeling van 1 tot 5 sterren.";
            } elseif (!isset($_GET['comment']) || str_word_count($_GET['comment']) < 5) {
                $output['status'] = "error";
                $output['message'] = "Vertel je ervaring in minimaal 5 woorden.";
            } elseif (!isset($_GET['captcha']) || strlen($_GET['captcha']) < 10) {
                $output['status'] = "error";
                $output['message'] = "Bevestig nog even dat je geen robot (spam) bent.";
            } else {

                $add = new RatingComment();
                $add->Name = $_GET['name'];
                $add->Email = $_GET['email'];
                $add->Rating = $_GET['rating'];
                $add->Comment = $_GET['comment'];
                $add->Ip = $_GET['ip'];
                $add->PageID = $_GET['pageid'];
                $add->write();

                $output['status'] = "success";
                $output['message'] = "Bedankt, de redactie werpt nog even een blik op je beoordeling en zal dan worden geplaatst. Dit is doorgaans binnen maximaal 24 uur.";

            }
            return json_encode($output);

        }

        return false;

    }

    public function RatingCommentsApproved(){

        return $this->owner->RatingComments()->filter('Enabled', true);

    }

}
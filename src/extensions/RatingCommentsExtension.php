<?php

namespace Hestec\RatingComments;

use SilverStripe\ORM\DataExtension;

class RatingCommentsExtension extends DataExtension {

    private static $has_many = array(
        'RatingComments' => RatingComment::class
    );

}
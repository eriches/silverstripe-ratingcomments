<?php

namespace Hestec\RatingComments;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\ORM\DataObject;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\FieldList;

class RatingComment extends DataObject {

    private static $singular_name = 'RatingComment';
    private static $plural_name = 'RatingComments';

    private static $table_name = 'HestecRatingComment';

    private static $db = array(
        'Name' => 'Varchar(100)',
        'Comment' => 'Text',
        'Rating' => 'Int',
        'Email' => 'Varchar(100)',
        'Ip' => 'Varchar(40)',
        'Enabled' => 'Boolean'
    );

    private static $default_sort='Created DESC';

    private static $has_one = array(
        'Page' => SiteTree::class
    );

    private static $summary_fields = array(
        'Created',
        'Name',
        'Enabled.Nice'
    );

    public function getCMSFields() {

        $NameField = TextField::create('Name', "Name");
        $CommentField = TextareaField::create('Comment', "Comment");
        $RatingField = NumericField::create('Rating', "Rating");
        $EmailField = EmailField::create('Email', "Email");
        $IpField = ReadonlyField::create('Ip', "Ip");
        $EnabledField = CheckboxField::create('Enabled', "Enabled");
        $PageField = ReadonlyField::create('PageUrl', "Page", $this->Page()->AbsoluteLink());

        return new FieldList(
            $EnabledField,
            $NameField,
            $CommentField,
            $RatingField,
            $EmailField,
            $PageField,
            $IpField
        );

    }

}
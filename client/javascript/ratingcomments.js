function RatingComment(email, name, rating, comment, captcha, pageid, ip, link, elem, elemform){

    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: link + 'AddRatingComment',
        data: {email: email, name: name, rating: rating, comment: comment, captcha: captcha, pageid: pageid, ip: ip},
        success: function (data) {
            if (data['status'] == 'success'){
                $(elem + ' .rc-message').empty();
                $(elem + ' .rc-message').removeClass('rc-error');
                $(elemform).hide();
                $(elem + ' .rc-message').append(data['message']);
            }else{
                $(elem + ' .rc-message').empty();
                $(elem + ' .rc-message').addClass('rc-error');
                $(elem + ' .rc-message').append(data['message']);
            }
        },
        error: function (data) {
            $(elem + ' .rc-message').empty();
            $(elem + ' .rc-message').addClass('rc-error');
            $(elem + ' .rc-message').prepend("Sorry, er is een onbekende fout opgetreden. Probeer het nog een keer.");
        }
    })

};

// possibly place this in an existing "document.ready"
$(document).ready(function() {

    $('#Form_RatingCommentForm_action_SubmitRatingCommentForm').click(function() {

        var email = $('#Form_RatingCommentForm_Email').val();
        var name = $('#Form_RatingCommentForm_Name').val();
        var captcha = $('#g-recaptcha-response').val();
        if ($('#Form_RatingCommentForm_Rating_1').is(':checked')){
            var rating = 1;
        }
        if ($('#Form_RatingCommentForm_Rating_2').is(':checked')){
            var rating = 2;
        }
        if ($('#Form_RatingCommentForm_Rating_3').is(':checked')){
            var rating = 3;
        }
        if ($('#Form_RatingCommentForm_Rating_4').is(':checked')){
            var rating = 4;
        }
        if ($('#Form_RatingCommentForm_Rating_5').is(':checked')){
            var rating = 5;
        }
        var comment = $('#Form_RatingCommentForm_Comment').val();
        var pageid = $('#Form_RatingCommentForm_PageId').val();
        var ip = $('#Form_RatingCommentForm_Ip').val();
        var link = $('#Form_RatingCommentForm_Link').val();
        var elem = '#rc-comment';
        var elemform = '#rc-form';

        RatingComment(email, name, rating, comment, captcha, pageid, ip, link, elem, elemform);
        return false;
    });

    $('ul.rate-vote').click(function() {

    });

});
